
#[macro_use] extern crate envconfig_derive;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate failure_derive;
#[macro_use] extern crate failure;
extern crate envconfig;
extern crate slog;
extern crate slog_async;
extern crate slog_term;
extern crate actix_web;
extern crate reqwest;
extern crate serde;

use envconfig::Envconfig;
use slog::Drain;
use slog::o;
use actix_web::{server, App, http};

mod handlers;
mod appstate;

use appstate::AppState;

#[derive(Deserialize)]
struct SignInResponse {}


#[derive(Envconfig)]
pub struct Config {
    #[envconfig(from = "API_KEY", default = "")]
    pub api_key: String,

    #[envconfig(from = "API_SECRET", default = "")]
    pub api_secret: String,
}

pub fn setup_logging() -> slog::Logger {
    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::CompactFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    slog::Logger::root(drain, o!())
}

// pub fn get_jwt(client: &reqwest::Client,
//                base_url: &str,
//                api_key: &str,
//                api_secret: &str) -> Result<String, Error> {
//     let mut body = HashMap::new();
//     body.insert("apiKey",  api_key);
//     body.insert("apiSecret", api_secret);
//     let jwt_path = format!("{}/developer/sign-in", base_url);
//     let result = client.post(&jwt_path)
//                        .json(&body)
//                        .send()?;
//     let json: Result<SignInResponse, Error> = serde_json::from_str(&result)
//         .map_err(|e| format_err!("could not parse json, reason: {}", e));
//     Ok(json.unwrap().token)
// }

fn main() {
    let config = match Config::init() {
        Ok(v) => v,
        Err(e) => panic!("Could not read config from environment: {}", e),
    };
    let base_url = "let";
    let client = reqwest::Client::new();
    let api_key = config.api_key;
    let api_secret = config.api_secret;
    // let jwt = match get_jwt(&client, &base_url, &api_key, &api_secret) {
    //     Ok(v) => v,
    //     Err(e) => panic!("Could no get the JWT: {}", e),
    // };
    let log = setup_logging();
    server::new(move || {
        App::with_state(AppState {
            // jwt: jwt.to_string(),
            log: log.clone(),
        })
        .scope("rest/v1", |v1_scope| {
            v1_scope.nested("/activites", |activities_scope| {
                activities_scope
                    .resource("", |r| {
                        r.method(http::Method::GET).f(handlers::get_activities);
                    })
            })
            // v1_scope.nested("/activities", |activities_scope| {
            //     activities_scope
            //         .resource("", |r| {
            //             r.method(http::Method::GET).f(handlers::get_activities);
            //             r.method(http::Method::POST)
            //                 .with_config(handlers::create_activity, |cfg| {
            //                     (cfg.0).1.error_handler(handlers::json_error_handler);
            //                 })
            //         })
            //         .resource("/{activity_id}", |r| {
            //             r.method(http::Method::GET).with(handlers::get_activity);
            //             r.method(http::Method::DELETE)
            //                 .with(handlers::delete_activity);
            //             r.method(http::Method::PATCH)
            //                 .with_config(handlers::edit_activity, |cfg| {
            //                     (cfg.0).1.error_handler(handlers::json_error_handler);
            //                 });
            //         })
            // })
        })
        .resource("/health", |r| {
            r.method(http::Method::GET).f(handlers::health)
        })
        .finish()
    })
    .bind("0.0.0.0:8080")
    .unwrap()
    .run();
}
