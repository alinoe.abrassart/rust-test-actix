
use failure::{Error, Fail};
use reqwest::Response;
use actix_web::{error, HttpResponse, HttpRequest, Json};

#[derive(Debug, Fail)]
pub enum HandlerError {
    #[fail(display = "invalid client request, cause: {}", cause)]
    WrongClientRequest {
        cause: String,
    },
    #[fail(display = "server error, cause: {}", cause)]
    ServerError {
        cause: String,
    },
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ActivityResponse;
#[derive(Serialize, Deserialize, Debug)]
pub struct HealthResponse {
    status: String
}

const BASE_URL: &str = "http://localhost:5000";

use crate::appstate::AppState;

fn parse_result(mut res: Response) -> Result<String, Error> {
    let mut buf: Vec<u8> = vec![];
    if res.status().is_success() {
        res.copy_to(&mut buf)?;
    } else {
        return Err(format_err!("request error: {}", res.status()));
    }
    let result = std::str::from_utf8(&buf)?;
    Ok(result.to_string())
}

fn get(path: &str) -> Result<Response, reqwest::Error>{
    let client = reqwest::Client::new();
    client.get(path).send()
}

impl error::ResponseError for HandlerError {
    fn error_response(&self) -> HttpResponse {
        match self {
            HandlerError::WrongClientRequest{cause} => HttpResponse::InternalServerError()
                .content_type("text/plain")
                .body(cause),
            HandlerError::ServerError{cause} => HttpResponse::NotFound()
                .content_type("text/plain")
                .body(cause),
        }
    }
}

impl From<reqwest::Error> for HandlerError {
    fn from(error: reqwest::Error) -> Self {
        if error.is_client_error() {
            HandlerError::WrongClientRequest {
                cause: error.name().unwrap_or(&"No known cause").to_string()
            }
        } else {
            HandlerError::ServerError {
                cause: error.name().unwrap_or(&"No known cause").to_string()
            }
        }
    }
}

pub fn get_activities(req: &HttpRequest<AppState>) -> Result<Json<ActivityResponse>, Error> {
    let activities_path = format!("{}/activities", BASE_URL);
    let result = parse_result(get(&activities_path)?)?;
    serde_json::from_str(&result)
        .map_err(|e| HandlerError::ServerError{cause: e.to_string()})
}

pub fn health(_: &HttpRequest<AppState>) -> Result<Json<HealthResponse>, HandlerError> {
    let response = HealthResponse{status: "Ok".to_owned()};
    Ok(Json(response))
}
